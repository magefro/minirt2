/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minirt.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: earruaba <earruaba@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/14 13:00:05 by alangloi          #+#    #+#             */
/*   Updated: 2021/12/13 15:58:01 by earruaba         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINIRT_H
# define MINIRT_H

# if defined (__APPLE__)

#  include <mlx.h>
#  define ESCKEY		53

# elif defined (__linux__)

#  include "../linux/mlx/mlx.h"
#  define ESCKEY		65307
#  define CAM_FRONT     119
#  define CAM_BACK      115
#  define CAM_LEFT      97
#  define CAM_RIGHT     100
#  define CAM_ROT_L     113
#  define CAM_ROT_R     101
#  define CAM_ROT_U     99
#  define CAM_ROT_D     122
#  define L_FRONT       116
#  define L_BACK        103
#  define L_LEFT        102
#  define L_RIGHT       104
#  define OBJ_FRONT     105
#  define OBJ_BACK      107
#  define OBJ_LEFT      106
#  define OBJ_RIGHT     108
#  define OBJ_ROT_L     117
#  define OBJ_ROT_R     111
#  define OBJ_ROT_U     46
#  define OBJ_ROT_D     109
#  define NEXT_OBJ      110
#  define SIZE_P        112
#  define SIZE_M        59
#  define LENGTH_P      91
#  define LENGTH_M      39

# endif

# include <fcntl.h>
# include <unistd.h>
# include <limits.h>
# include <stdio.h>
# include <stdlib.h>
# include <math.h>

# define BUFFER_SIZE 32
# define SP 1
# define CY 2
# define PL 3

typedef struct s_vector
{
	double 	        x;
	double 	        y;
	double 	        z;
}				t_vector;

typedef struct s_color
{
	double          r;
	double          g;
	double          b;
}				t_color;

typedef struct s_light
{
	t_vector	    pos;
	double		    intensity;
	t_color		    col;
}				t_light;

typedef struct s_ambient
{
	double 		    intensity;
	t_color		    col;
}				t_ambient;

typedef struct s_object
{
	int 			id;
	void 			*obj;
	struct s_object	*next;
}				t_object;

typedef struct s_plane
{
	t_vector		pos;
	t_vector		dir;
	t_vector		normal;
	t_color			col;
}				t_plane;

typedef struct s_cylinder
{
	t_vector		pos;
	t_vector		pos2;
	t_vector		dir;
	t_vector    	rot;
    t_vector        delta;
    t_vector        root;
	float 			d;
	float 			h;
	t_color			col;
}				t_cylinder;

typedef struct s_sphere
{
	t_vector	    pos;
	double		    rad;
	t_color		    col;
}				t_sphere;

typedef struct s_inter
{
	double			t1;
	double 			t2;
}				t_inter;

typedef struct s_camera
{
    t_vector    pos;
    t_vector    rot;
    t_vector    dir_x;
    t_vector    dir_z;
    t_vector    dir_y;
    double      fov;
}               t_camera;

typedef struct s_ray
{
	t_vector	    dir;
	t_vector	    ori;
	t_vector	    point;
	t_vector	    normal;
	double		    t;
	t_inter			i;
	t_color			pixel;
	double 			closest;
	t_object		*closest_obj;
}				t_ray;

typedef struct s_scene
{
	double 			h;
	double			w;
    double          vp_w;
    double          vp_h;
    double          vp_d;
	double 			x;
	double 			y;
	void			*mlx_ptr;
	void			*img_ptr;
	void			*win_ptr;
	unsigned char	*img_data;
	int 			bpp;
	int 			size_line;
	int 			endian;
	t_camera        cam;
	t_light			l;
	t_ambient		a;
	t_object		*obj;
	t_object		**head;
	int 			max;
	int 			cur;
}				t_scene;

/* parsing */
int 		parse(char *file, t_scene *rt);
int			ft_is_rt(char *file);

/* error */
int			error_message(char *msg);

/* display */
void		display(t_scene *rt);
void		print_screen(t_scene *rt, t_color *pixel);

/* intersection */
void 		intersection(t_ray *r, t_scene *rt);

/* parse lights */
int			parse_light(char **tab, t_scene *rt);
int 		parse_ambient(char **tab, t_scene *rt);

/* parse spheres */
t_sphere	*alloc_sphere(char **tab);
int			parse_sphere(char **tab, t_scene *rt);

/* parse cylinders */
int			parse_cylinder(char **tab, t_scene *rt);
t_cylinder	*alloc_cylinder(char **tab);

/* parse plan */
int			parse_plane(char **tab, t_scene *rt);
t_plane		*alloc_plane(char **tab);

/* parse camera */
int       	parse_camera(char **tab, t_scene *rt);
void		calculate_cam_rot(t_camera *cam);

/* print help */
void        print_help_1(void);
void        print_help_2(void);

/* objects */
int			add_object(void *new, t_object **list, int id);
int         count_object_list(t_scene *rt);
int 		intersect_object(t_ray *r, t_object *cur);

/* object intersection */
int         intersect_plane(t_ray *r, t_plane *p);
int         intersect_sphere(t_ray *r, t_sphere *s);
int         intersect_cylinder(t_ray *r, t_cylinder *c);

/* normals */
t_vector	normal_obj(t_ray *r);

/* colors */
void 		get_surface_col(t_object *o, t_color *pixel);

/* lights */
void		get_light(t_ray *r, t_scene *rt);
void        adding_lum(t_color *col, double lum, t_color light_col);
void        adding_light(t_color *col, double new, t_color light_col);

/* rotation */
void 		calculate_rotate(t_vector *v, t_vector deg);

/* viewport */
double      viewport_y(int y, t_scene *rt);
double      viewport_x(int x, t_scene *rt);
int         center_y(int y, t_scene *rt);
int         center_x(int x, t_scene *rt);

/* color operations */
t_color		init_color(double r, double g, double b);
t_vector	col_to_vec(t_color col);
t_color		vec_to_col(t_vector vec);
t_color		add_color(t_color c1, t_color c2);
t_color		div_color_div(double c, t_color col);
t_color		mult_color(t_color c1, t_color c2);
t_color		mult_color_prod(double c, t_color col);
t_color		dot_color(t_color col);
t_color		sqrt_color(t_color col);

/* get next line */
int			get_next_line(int fd, char **line);

/* vector op */
void 		init_vector(t_vector *v, double x, double y, double z);
t_vector	add_vector(t_vector v1, t_vector v2);
t_vector	mult_vector(t_vector v1, t_vector v2);
t_vector	mult_vector_prod(double c, t_vector v);
t_vector	sub_vector(t_vector v1, t_vector v2);
t_vector	div_vector(t_vector v1, t_vector v2);
t_vector	div_vector_div(double c, t_vector v);
double		dot_vector(t_vector v1, t_vector v2);
double		norm_vector(t_vector v);
t_vector	normalize(t_vector v);
t_vector	cross_vector(t_vector v1, t_vector v2);

/* split */
char		**ft_split(char const *s, char c);

/* atoi double */
double		ft_atoi_double(char *str);

/* utils */
char		*ft_strdup(const char *s1);
char		*ft_substr(char const *s, unsigned int start, size_t len);
char		*ft_strjoin(char const *s1, char const *s2);
size_t		ft_strlen(const char *s);
char		*ft_strchr(const char *s, int c);
void		ft_strdel(char **as);
int			ft_atoi(const char *s);
int			ft_strcmp(const char *s1, const char *s2);
void		*ft_memcpy(void *dst, const void *src, size_t n);
int			ft_pow(int x, unsigned int y);
double		minmax(double val);
void 		normalized(t_vector *v);
double      rad(double n);
double      rotate_degres(double rot);

/* frees */
int			free_obj(t_scene *rt);
int			free_tab(char **tab);
int			free_mlx(t_scene *rt);
int			free_image(t_scene *rt);
int			free_win(t_scene *rt);

/* hooks */
int			key_hook(int keycode, t_scene *rt);
int			close_window(int keycode, t_scene *rt);
void        plane_modify(t_object *tmp, double i, double j, int keycode);
void        plane_moves(t_plane *pla, double i, double j, int keycode);
void        cylinder_modify(t_object *tmp, double i, double j, int keycode);
void        cylinder_moves(t_cylinder *cyl, double i, double j, int keycode);
void        sphere_modify(t_object *tmp, double i, double j, int keycode);
int 	    obj_key_modify(int keycode, t_scene *rt);
int		    close_window(int keycode, t_scene *rt);
int         key_exit(int keycode, t_scene *rt);

#endif