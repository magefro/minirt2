/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   intersection.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/15 11:14:13 by alangloi          #+#    #+#             */
/*   Updated: 2021/12/15 11:18:07 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

int 		intersect_object(t_ray *r, t_object *cur)
{
	if (cur->id == SP)
		intersect_sphere(r, (t_sphere *)cur->obj);
	if (cur->id == CY)
		intersect_cylinder(r, (t_cylinder *)cur->obj);
	if (cur->id == PL)
		intersect_plane(r, (t_plane *)cur->obj);
	return (0);
}

void		intersection(t_ray *r, t_scene *rt)
{
	t_object	*cur;

	r->closest = __DBL_MAX__;
	r->closest_obj = NULL;
	cur = rt->obj;
	printf("oy\n");
	while (cur)
	{
		printf("1\n");
		intersect_object(r, cur);
		printf("2\n");
		if (r->i.t1 > 0 && r->i.t1 < __DBL_MAX__ && r->i.t1 < r->closest)
		{
			r->closest = r->i.t1;
			r->closest_obj = cur;
		}
		printf("3\n");
		if (r->i.t2 > 0 && r->i.t2 < __DBL_MAX__ && r->i.t2 < r->closest)
		{
			r->closest = r->i.t2;
			r->closest_obj = cur;
		}
		printf("4\n");
		if (r->closest_obj != NULL)
		{
			get_surface_col(r->closest_obj, &r->pixel);
		}
		printf("5\n");
		cur = cur->next;
		printf("6\n");
	}
    r->point = add_vector(r->ori, mult_vector_prod(r->closest, r->dir));
	printf("7\n");
    r->normal = normal_obj(r);
    printf("dfgdfgdfgdfdfgdfgdfgdfgdfgdfgdfgdfgdfgdoy\n");
	get_light(r, rt);
    adding_lum(&r->pixel, rt->a.intensity, rt->a.col);
}
