//
// Created by alangloi on 12/13/21.
//

#include "minirt.h"

int     free_obj(t_scene *rt)
{
    t_object *cur;
    t_object *tmp;

    cur = rt->obj;
    while (cur)
    {
        tmp = cur;
        cur = cur->next;
		free(tmp->obj);
		tmp->obj = NULL;
        free(tmp);
        tmp = NULL;
    }
    return (1);
}

int		free_tab(char **tab)
{
	int		i;

	i = 0;
	while (tab[i])
	{
		free(tab[i]);
		tab[i] = NULL;
		i++;
	}
	free(tab);
	tab = NULL;
	return (0);
}

int		free_image(t_scene *rt)
{
	if (rt->img_ptr)
		mlx_destroy_image(rt->mlx_ptr, rt->img_ptr);
	return (0);
}

int		free_win(t_scene *rt)
{
	if (rt->win_ptr)
	{
		mlx_clear_window(rt->mlx_ptr, rt->win_ptr);
		mlx_destroy_window(rt->mlx_ptr, rt->win_ptr);
		rt->win_ptr = NULL;
	}
	return (0);
}

int		free_mlx(t_scene *rt)
{
	free_image(rt);
	free_win(rt);
	if (rt->mlx_ptr)
	{
		mlx_destroy_display(rt->mlx_ptr);
		free(rt->mlx_ptr);
		rt->mlx_ptr = NULL;
	}
	return (0);
}