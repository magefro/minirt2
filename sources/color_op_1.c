//
// Created by Antoine LANGLOIS on 9/16/21.
//

#include "minirt.h"

t_color		init_color(double r, double g, double b)
{
	t_color	col;

	col.r = r;
	col.g = g;
	col.b = b;
	return (col);
}

t_vector	col_to_vec(t_color col)
{
	t_vector vec;

	vec.x = col.r;
	vec.y = col.g;
	vec.z = col.b;
	return (vec);
}

t_color		vec_to_col(t_vector vec)
{
	t_color	col;

	col.r = vec.x;
	col.g = vec.y;
	col.b = vec.z;
	return (col);
}

t_color		add_color(t_color c1, t_color c2)
{
	t_color	result;

	result.r = c1.r + c2.r;
	result.g = c1.g + c2.g;
	result.b = c1.b + c2.b;
	return (result);
}

t_color		div_color_div(double c, t_color col)
{
	t_color	result;

	result.r = col.r / c;
	result.g = col.g / c;
	result.b = col.b / c;
	return (result);
}

