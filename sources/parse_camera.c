//
// Created by user42 on 12/12/2021.
//

#include "minirt.h"

void  calculate_cam_rot(t_camera *cam)
{
    cam->dir_x.x = 1;
    cam->dir_x.y = 0;
    cam->dir_x.z = 0;
    cam->dir_y.x = 0;
    cam->dir_y.y = 1;
    cam->dir_y.z = 0;
    cam->dir_z.x = 0;
    cam->dir_z.y = 0;
    cam->dir_z.z = 1;
    calculate_rotate(&cam->dir_x, cam->rot);
    calculate_rotate(&cam->dir_y, cam->rot);
    calculate_rotate(&cam->dir_z, cam->rot);
}

int parse_camera(char **tab, t_scene *rt)
{
    t_camera new;
    char **crd;
    char **rot;

    crd = ft_split(tab[1], ',');
    rot = ft_split(tab[2], ',');
	new.fov = ft_atoi_double(tab[3]);
	if (new.fov < 0)
	{
		error_message("Wrong argument as fov.\n");
		return (0);
	}
    new.pos.x = ft_atoi_double(crd[0]);
    new.pos.y = ft_atoi_double(crd[1]);
    new.pos.z = ft_atoi_double(crd[2]);
    new.rot.x = ft_atoi_double(rot[0]);
    new.rot.y = ft_atoi_double(rot[1]);
    new.rot.z = ft_atoi_double(rot[2]);
	if (new.rot.x < -1.0000 || new.rot.x > 1.0000
		|| new.rot.y < -1.0000 || new.rot.y > 1.0000
		|| new.rot.z < -1.0000 || new.rot.z > 1.0000)
	{
		error_message("Wrong argument as rot.\n");
		free_tab(crd);
		free_tab(rot);
		return (0);
	}
    calculate_cam_rot(&new);
    rt->cam = new;
	free_tab(crd);
	free_tab(rot);
	return (1);
}
