//
// Created by alangloi on 12/8/21.
//

#include "minirt.h"

static void get_cylinder_col(t_cylinder *c, t_color *pixel)
{

	pixel->r = c->col.r;
	pixel->g = c->col.g;
	pixel->b = c->col.b;
}

static void get_sphere_col(t_sphere *s, t_color *pixel)
{
	pixel->r = s->col.r;
	pixel->g = s->col.g;
	pixel->b = s->col.b;
}

static void get_plane_col(t_plane *p, t_color *pixel)
{
    pixel->r = p->col.r;
    pixel->g = p->col.g;
    pixel->b = p->col.b;
}

void 	get_surface_col(t_object *o, t_color *pixel)
{
	if (o->id == SP)
	{
		get_sphere_col((t_sphere *)o->obj, pixel);
		return ;
	}
	if (o->id == CY)
	{
		get_cylinder_col((t_cylinder *)o->obj, pixel);
		return ;
	}
    if (o->id == PL)
    {
        get_plane_col((t_plane *)o->obj, pixel);
        return ;
    }
	pixel->r = 255;
	pixel->g = 255;
	pixel->b = 255;
}