//
// Created by Antoine LANGLOIS on 9/16/21.
//

#include "minirt.h"

int parse_sphere(char **tab, t_scene *rt)
{
	t_sphere *new;

	new = alloc_sphere(tab);
	if (!new)
	{
		error_message("Wrong argument when allocating sphere.\n");
		return (0);
	}
	if (!add_object((t_sphere *)new, &rt->obj, SP))
	{
		error_message("Can't add sphere to scene.\n");
		return (0);
	}
	return (1);
}

t_sphere *alloc_sphere(char **tab)
{
	t_sphere    *s;
	char 		**crd;
	char		**rgb;

	s = malloc(sizeof(t_sphere));
	if (!s)
		return (NULL);
	s->rad = ft_atoi_double(tab[2]);
	if (s->rad < 0)
		return (NULL);
	crd = ft_split(tab[1], ',');
	rgb = ft_split(tab[3], ',');
	s->pos.x = ft_atoi_double(crd[0]);
	s->pos.y = ft_atoi_double(crd[1]);
	s->pos.z = ft_atoi_double(crd[2]);
	s->col.b = ft_atoi_double(rgb[0]);
	s->col.g = ft_atoi_double(rgb[1]);
	s->col.r = ft_atoi_double(rgb[2]);
	if (s->col.b < 0 || s->col.b > 255 || s->col.g < 0 || s->col.g > 255
		|| s->col.r < 0 || s->col.r > 255)
	{
		free_tab(crd);
		free_tab(rgb);
		return (NULL);
	}
    free_tab(crd);
	free_tab(rgb);
	return (s);
}