//
// Created by Antoine LANGLOIS on 9/14/21.
//

#include "minirt.h"

t_vector        div_vector(t_vector v1, t_vector v2)
{
	t_vector    result;

	result.x = v1.x / v2.x;
	result.y = v1.y / v2.y;
	result.z = v1.z / v2.z;
	return (result);
}

t_vector        div_vector_div(double c, t_vector v)
{
	t_vector    result;

	result.x = v.x / c;
	result.y = v.y / c;
	result.z = v.z / c;
	return (result);
}

double          dot_vector(t_vector v1, t_vector v2)
{
	return (v1.x * v2.x + v1.y * v2.y + v1.z * v2.z);
}

double          norm_vector(t_vector v)
{
	return (sqrt(dot_vector(v, v)));
}

t_vector       normalize(t_vector v)
{
	double		norm;

	norm = norm_vector(v);
	v.x /= norm;
	v.y /= norm;
	v.z /= norm;
	return (v);
}

t_vector	cross_vector(t_vector v1, t_vector v2)
{
	t_vector vec;

	init_vector(&vec, v1.y * v2.z - v1.z * v2.y, v1.z * v2.x - v1.x * v2.z, v1.x * v2.y - v1.y * v2.x);
	return (vec);
}