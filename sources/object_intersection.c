//
// Created by user42 on 14/12/2021.
//

#include "minirt.h"

int intersect_plane(t_ray *r, t_plane *p)
{
    double d;

    d = dot_vector(r->dir, p->normal);
    r->i.t2 = __DBL_MAX__;
    if (fabs(d) > 0.000001)
    {
        r->i.t1 = dot_vector(sub_vector(p->pos, r->ori), p->normal) / d;
        if (r->i.t1 <= 0)
            r->i.t1 = __DBL_MAX__;
    }
    else
        r->i.t1 = __DBL_MAX__;
    return (0);
}

int intersect_sphere(t_ray *r, t_sphere *s)
{
    double		vec1;
    double		vec2;
    double		vec3;
    double		discr;
    t_vector	dist;

    vec1 = dot_vector(r->dir, r->dir);
    dist = sub_vector(r->ori, s->pos);
    vec2 = 2 * dot_vector(r->dir, dist);
    vec3 = dot_vector(dist, dist) - (s->rad * s->rad);
    discr = vec2 * vec2 - 4 * vec1 * vec3;
    if (discr < 0)
    {
        r->i.t1 = __DBL_MAX__;
        r->i.t2 = __DBL_MAX__;
    }
    else
    {
        r->i.t1 = (-vec2 + sqrt(discr)) / (2 * vec1);
        r->i.t2 = (-vec2 - sqrt(discr)) / (2 * vec1);
    }
    return (0);
}

static void set_params_cylinder_1(t_cylinder *c, t_ray *r)
{
    t_vector	vec1;
    t_vector	vec2;
    t_vector	vec3;

    vec1 = sub_vector(r->dir, mult_vector_prod(dot_vector(c->dir, r->dir), c->dir));
    vec2 = sub_vector(r->ori, c->pos);
    vec3 = sub_vector(vec2, mult_vector_prod(dot_vector(vec2, c->dir), c->dir));
    r->i.t1 = __DBL_MAX__;
    r->i.t2 = __DBL_MAX__;
    c->root.x = dot_vector(vec1, vec1);
    c->root.y = 2 * dot_vector(vec1, vec3);
    c->root.z = dot_vector(vec3, vec3) - c->d * c->d;
    c->delta.x = (c->root.y * c->root.y) - 4 * c->root.x * c->root.z;
}

static void set_params_cylinder_2(t_cylinder *c, t_ray *r)
{
    t_vector	vec1;
    t_vector	vec2;

    c->delta.y = (-c->root.y - sqrt(c->delta.x)) / (2 * c->root.x);
    c->delta.z = (-c->root.y + sqrt(c->delta.x)) / (2 * c->root.x);
    vec1 = add_vector(r->ori, mult_vector_prod(c->delta.y, r->dir));
    vec2 = add_vector(r->ori, mult_vector_prod(c->delta.z, r->dir));
    if (c->delta.y > 0.00001 && dot_vector(c->dir, sub_vector(vec1, c->pos)) > 0 && dot_vector(mult_vector_prod(-1, c->dir), sub_vector(vec1, c->pos2)) > 0)
        r->i.t1 = c->delta.y;
    else
    {
        if (c->delta.z > 0.00001 && dot_vector(c->dir, sub_vector(vec2, c->pos)) > 0 && dot_vector(mult_vector_prod(-1, c->dir), sub_vector(vec2, c->pos2)) > 0)
            r->i.t2 = c->delta.z;
        else
            r->i.t2 = __DBL_MAX__;
    }
}

int	intersect_cylinder(t_ray *r, t_cylinder *c)
{
    set_params_cylinder_1(c, r);
    if (c->delta.x < 0)
    {
        r->i.t1 = __DBL_MAX__;
        r->i.t2 = __DBL_MAX__;
    }
    else
       set_params_cylinder_2(c, r);
    return (0);
}