//
// Created by alangloi on 12/8/21.
//

#include "minirt.h"

int parse_plane(char **tab, t_scene *rt)
{
	t_plane *new;

	new = alloc_plane(tab);
	if (!new)
	{
		error_message("Wrong argument when allocating plane.\n");
		return (0);
	}
	if (!add_object((t_plane *)new, &rt->obj, PL))
	{
		error_message("Can't add plane to scene.\n");
		return (0);
	}
	return (1);
}

t_plane *alloc_plane(char **tab)
{
	t_plane *p;
	char	**crd;
	char 	**rgb;
	char 	**dir;

	p = malloc(sizeof(t_plane));
	if (!p)
		return (NULL);
	crd = ft_split(tab[1], ',');
	dir = ft_split(tab[2], ',');
	rgb = ft_split(tab[3], ',');
	p->pos.x = ft_atoi_double(crd[0]);
	p->pos.y = ft_atoi_double(crd[1]);
	p->pos.z = ft_atoi_double(crd[2]);
	p->dir.x = ft_atoi_double(dir[0]);
	p->dir.y = ft_atoi_double(dir[1]);
	p->dir.z = ft_atoi_double(dir[2]);
	p->col.b = ft_atoi_double(rgb[0]);
	p->col.g = ft_atoi_double(rgb[1]);
	p->col.r = ft_atoi_double(rgb[2]);
	if (p->col.b < 0 || p->col.b > 255 || p->col.g < 0 || p->col.g > 255
		|| p->col.r < 0 || p->col.r > 255 || p->dir.x < -1.0000
		|| p->dir.x > 1.0000 || p->dir.y < -1.0000 || p->dir.y > 1.0000
		|| p->dir.z < -1.0000 || p->dir.z > 1.0000)
	{
		free_tab(crd);
		free_tab(rgb);
		free_tab(dir);
		return (NULL);
	}
	init_vector(&p->normal, 0, 1, 0);
	calculate_rotate(&p->normal, p->dir);
    free_tab(crd);
    free_tab(dir);
	free_tab(rgb);
	return (p);
}