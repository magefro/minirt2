//
// Created by user42 on 14/12/2021.
//

#include "minirt.h"

void print_help_1(void)
{
    printf("\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    printf("+                                                         +\n");
    printf("+                  42 MINIRT 12/2021                      +\n");
    printf("+                                                         +\n");
    printf("+ LIST OF COMMANDS :                                      +\n");
    printf("+                                                         +\n");
    printf("+     Camera movements - W = MOVE FRONT                   +\n");
    printf("+                      - A = MOVE LEFT                    +\n");
    printf("+                      - S = MOVE BACK                    +\n");
    printf("+                      - D = MOVE RIGHT                   +\n");
    printf("+                                                         +\n");
    printf("+                      - Q = ROTATE LEFT                  +\n");
    printf("+                      - E = ROTATE RIGHT                 +\n");
    printf("+                      - Z = ROTATE UP                    +\n");
    printf("+                      - C = ROTATE DOWN                  +\n");
    printf("+                                                         +\n");
    printf("+     Light movements  - T = MOVE FRONT                   +\n");
    printf("+                      - F = MOVE LEFT                    +\n");
    printf("+                      - G = MOVE BACK                    +\n");
    printf("+                      - H = MOVE RIGHT                   +\n");
    printf("+                                                         +\n");
    printf("+     Objects modify   - I = MOVE FRONT                   +\n");
}

void     print_help_2(void)
{
    printf("+                      - J = MOVE LEFT                    +\n");
    printf("+                      - K = MOVE BACK                    +\n");
    printf("+                      - L = MOVE RIGHT                   +\n");
    printf("+                                                         +\n");
    printf("+                      - U = ROTATE LEFT                  +\n");
    printf("+                      - O = ROTATE RIGHT                 +\n");
    printf("+                      - M = ROTATE UP                    +\n");
    printf("+                      - . = ROTATE DOWN                  +\n");
    printf("+                                                         +\n");
    printf("+                      - N = NEXT OBJECT                  +\n");
    printf("+                                                         +\n");
    printf("+                      - P = OBJECT SIZE +                +\n");
    printf("+                      - ; = OBJECT SIZE -                +\n");
    printf("+                                                         +\n");
    printf("+                      - [ = OBJECT LENGTH +              +\n");
    printf("+                      - ' = OBJECT LENGTH -              +\n");
    printf("+                                                         +\n");
    printf("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n\n");
}