//
// Created by Antoine LANGLOIS on 9/14/21.
//

#include "minirt.h"

int	ft_atoi(const char *s)
{
	int	res;
	int	neg;

	neg = 1;
	res = 0;
	while (*s && (*s == ' ' || *s == '\n' || *s == '\t' || *s == '\v'
				  || *s == '\f' || *s == '\r'))
		++s;
	if (*s == '-')
		neg = -1;
	if (*s == '-' || *s == '+')
		++s;
	while (*s && *s >= '0' && *s <= '9')
	{
		res = res * 10 + (*s - 48);
		++s;
	}
	return (res * neg);
}

int	ft_strcmp(const char *s1, const char *s2)
{
	const unsigned char	*p1;
	const unsigned char	*p2;
	unsigned char		c1;
	unsigned char		c2;

	p1 = (const unsigned char *)s1;
	p2 = (const unsigned char *)s2;
	c1 = *p1;
	c2 = *p2;
	while (c1 == c2)
	{
		c1 = (unsigned char)*p1++;
		c2 = (unsigned char)*p2++;
		if (c1 == '\0')
			return (c1 - c2);
	}
	return (c1 - c2);
}

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	unsigned char		*d;
	unsigned char		*s;

	d = (unsigned char *)dst;
	s = (unsigned char *)src;
	if (d == NULL && s == NULL)
		return (NULL);
	while (n--)
		*d++ = *s++;
	return (dst);
}

int	ft_pow(int x, unsigned int y)
{
	int	temp;

	if (y == 0)
		return (1);
	temp = ft_pow(x, y / 2);
	if (y % 2 == 0)
		return (temp * temp);
	else
		return (x * temp * temp);
}

double minmax(double val)
{
	if (val >= 255.0)
		val = 255.0;
	if (val <= 0.0)
		val = 0.0;
	return (val);
}