/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_hook.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/14 12:40:34 by alangloi          #+#    #+#             */
/*   Updated: 2021/12/14 15:47:12 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

int     cam_key_position(int keycode, t_scene *rt)
{
	double i;

	i = 10;
	if ((keycode == CAM_FRONT) || (keycode == CAM_BACK)
		|| (keycode == CAM_LEFT) || (keycode == CAM_RIGHT))
	{
		if (keycode == CAM_FRONT)
			rt->cam.pos = add_vector(rt->cam.pos, mult_vector_prod(i, rt->cam.dir_z));
		if (keycode == CAM_BACK)
			rt->cam.pos = sub_vector(rt->cam.pos, mult_vector_prod(i, rt->cam.dir_z));
		if (keycode == CAM_LEFT)
			rt->cam.pos = sub_vector(rt->cam.pos, mult_vector_prod(i, rt->cam.dir_x));
		if (keycode == CAM_RIGHT)
			rt->cam.pos = add_vector(rt->cam.pos, mult_vector_prod(i, rt->cam.dir_x));
		display(rt);
		mlx_put_image_to_window(rt->mlx_ptr, rt->win_ptr, rt->img_ptr, 0, 0);
	}
	return (0);
}

int     cam_key_rotation(int keycode, t_scene *rt)
{
	double i;

	i = 0.1;
	if ((keycode == CAM_ROT_L) || (keycode == CAM_ROT_R)
		|| (keycode == CAM_ROT_D) || (keycode == CAM_ROT_U))
	{
		if (keycode == CAM_ROT_L)
			rt->cam.rot.y -= i;
		if (keycode == CAM_ROT_R)
			rt->cam.rot.y += i;
		if (keycode == CAM_ROT_D)
			rt->cam.rot.x -= i;
		if (keycode == CAM_ROT_U)
			rt->cam.rot.x += i;
		calculate_cam_rot(&rt->cam);
		display(rt);
		mlx_put_image_to_window(rt->mlx_ptr, rt->win_ptr, rt->img_ptr, 0, 0);
	}
	return (0);
}

int 	light_key_position(int keycode, t_scene *rt)
{
	double i;

	i = 10;
	if ((keycode == L_FRONT) || (keycode == L_LEFT)
		|| (keycode == L_BACK) || (keycode == L_RIGHT))
	{
		if (keycode == L_FRONT)
			rt->l.pos.z += i;
		if (keycode == L_BACK)
			rt->l.pos.z -= i;
		if (keycode == L_LEFT)
			rt->l.pos.x -= i;
		if (keycode == L_RIGHT)
			rt->l.pos.x += i;
		display(rt);
		mlx_put_image_to_window(rt->mlx_ptr, rt->win_ptr, rt->img_ptr, 0, 0);
	}
	return (0);
}

int key_hook(int keycode, t_scene *rt)
{
	key_exit(keycode, rt);
	cam_key_rotation(keycode, rt);
	cam_key_position(keycode, rt);
	light_key_position(keycode, rt);
	obj_key_modify(keycode, rt);
	return (0);
}

/* Move camera
 * W = CAM_FRONT
 * A = CAM_LEFT
 * S = CAM_BACK
 * D = CAM_RIGHT
 *
 * Rotate Camera
 * Q = CAM_ROT_L
 * E = CAM_ROT_R
 * Z = CAM_ROT_D
 * C = CAM_ROT_U
 *
 * Move light
 * T = L_FRONT
 * F = L_LEFT
 * G = L_BACK
 * H = L_RIGHT
 *
 * Move Object
 * I = OBJ_FRONT
 * J = OBJ_LEFT
 * K = OBJ_BACK
 * L = OBJ_RIGHT
 *
 * Rotate Object
 * U = OBJ_ROT_L
 * O = OBJ_ROT_R
 * M = OBJ_ROT_D
 * > = OBJ_ROT_U
 *
 * Next Object
 * N = NEXT_OBJ
 *
 * Size Object
 * P = SIZE_P
 * : = SIZE_M
 *
 * Length Object
 * { = LENGTH_P
 * " = LENGTH_M
 *
 */
