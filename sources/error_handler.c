/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_handler.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: earruaba <earruaba@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/13 15:18:21 by earruaba          #+#    #+#             */
/*   Updated: 2021/12/13 16:00:55 by earruaba         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minirt.h"

static void	ft_putstr_fd(char *s, int fd)
{
    int ret;

    ret = 0;
	if (fd && s)
    {
        while (*s)
        {
            ret = write(fd, s++, 1);
        }
    }
    if (ret == -1)
    {
        printf("error write\n");
        exit (0);
    }

}

static int	ft_strncmp(const char *s1, const char *s2, size_t n)
{
	size_t	i;

	i = 0;
	while ((s1[i] || s2[i]) && i < n)
	{
		if (s1[i] != s2[i])
			return ((unsigned char)s1[i] - (unsigned char)s2[i]);
		i++;
	}
	return (0);
}

int	ft_is_rt(char *file)
{
	int	len;

	len = ft_strlen(file);
	if (file == 0)
		return (0);
	if (len < 4)
		return (0);
	if (ft_strncmp(file + len - 3, ".rt", 3) != 0)
		return (0);
	return (1);
}

int		error_message(char *msg)
{
	ft_putstr_fd("Error\n", 2);
	ft_putstr_fd(msg, 2);
	return (1);
}