//
// Created by user42 on 14/12/2021.
//

#include "minirt.h"

int     key_exit(int keycode, t_scene *rt)
{
    (void)rt;
    if (keycode == ESCKEY)
    {
        free_mlx(rt);
        free_obj(rt);
        exit(0);
    }
    return (0);
}

int		close_window(int keycode, t_scene *rt)
{
    (void)keycode;
    (void)rt;
    free_mlx(rt);
    exit(0);
}