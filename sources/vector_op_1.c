//
// Created by Antoine LANGLOIS on 9/14/21.
//

#include "minirt.h"

void init_vector(t_vector *v, double x, double y, double z)
{
	v->x = x;
	v->y = y;
	v->z = z;
}

t_vector        add_vector(t_vector v1, t_vector v2)
{
	t_vector    result;

	result.x = v1.x + v2.x;
	result.y = v1.y + v2.y;
	result.z = v1.z + v2.z;
	return (result);
}

t_vector        mult_vector(t_vector v1, t_vector v2)
{
	t_vector    result;

	result.x = v1.x * v2.x;
	result.y = v1.y * v2.y;
	result.z = v1.z * v2.z;
	return (result);
}

t_vector        mult_vector_prod(double c, t_vector v)
{
	t_vector    result;

	result.x = v.x * c;
	result.y = v.y * c;
	result.z = v.z * c;
	return (result);
}

t_vector        sub_vector(t_vector v1, t_vector v2)
{
	t_vector    result;

	result.x = v1.x - v2.x;
	result.y = v1.y - v2.y;
	result.z = v1.z - v2.z;
	return (result);
}
