#include "minirt.h"

void 	rotate_x(t_vector *dir, double angle)
{
	t_vector tmp;

	tmp.x = (1 * dir->x) + (0 * dir->y) + (0 * dir->z);
	tmp.y = (0 * dir->x) + (cos(angle) * dir->y) + (-sin(angle) * dir->z);
	tmp.z = (0 * dir->x) + (sin(angle) * dir->y) + (cos(angle) * dir->z);
	*dir = tmp;
}


void	rotate_y(t_vector *dir, double angle)
{
	t_vector tmp;

	tmp.x = (cos(angle) * dir->x) + (0 * dir->y) + (sin(angle) * dir->z);
	tmp.y = (0 * dir->x) + (1 * dir->y) + (0 * dir->z);
	tmp.z = (-sin(angle) * dir->x) + (0 * dir->y) + (cos(angle) * dir->z);
	*dir = tmp;
}

void 	rotate_z(t_vector *dir, double angle)
{
	t_vector	tmp;

	tmp.x = (cos(angle) * dir->x) + (-sin(angle) * dir->y) + (0 * dir->z);
	tmp.y = (sin(angle) * dir->x) + (cos(angle) * dir->y) + (0 * dir->z);
	tmp.z = (0 * dir->x) + (0 * dir->y) + (1 * dir->z);
    *dir = tmp;
}




void 	calculate_rotate(t_vector *v, t_vector deg)
{
	rotate_x(v, rad(rotate_degres(deg.x)));
	rotate_y(v, rad(rotate_degres(deg.y)));
	rotate_z(v, rad(rotate_degres(deg.z)));
}